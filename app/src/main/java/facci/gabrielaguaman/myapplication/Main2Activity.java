package facci.gabrielaguaman.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class Main2Activity extends AppCompatActivity {

    TextView txtr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        txtr = (TextView)findViewById(R.id.txtr);
        Bundle bundle = this.getIntent().getExtras();
        txtr.setText(bundle.getString("dato"));
    }
}
