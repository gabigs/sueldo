package facci.gabrielaguaman.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    EditText edtcs;
    TextView txtr;
    Button btnc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edtcs = (EditText) findViewById(R.id.edtcs);
        txtr = (TextView)findViewById(R.id.txtr);
        btnc = (Button)findViewById(R.id.btnc);
        btnc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Main2Activity.class);
                Bundle bundle = new Bundle();

                float valor = Float.parseFloat(edtcs.getText().toString());
                if (valor <=160){
                    float sueldo = (float) ((valor * 3));
                    txtr.setText(String.valueOf(sueldo));
                }else {
                    float sueldo = (float) ((valor * 3.25));
                    txtr.setText(String.valueOf(sueldo));
                }

                //float sueldo = (float) ((valor * 3));
                //txtr.setText(String.valueOf(sueldo));
                bundle.putString("dato", txtr.getText().toString());
                intent.putExtras(bundle);
                startActivity(intent);
                finish();
                Log.e("CF","Conversion Finalizada");

            }
        });
    }
}
